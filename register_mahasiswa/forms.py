from django import forms
from .models import *

class RegisterFormMahasiswa(forms.Form):
    error_message = {
        'required': 'This field is required to fill',
    }

    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Username',
    }

    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter Password',
    }

    npm = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter NPM',
    }
 
    email = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Email',
    }

    nama = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Full Name',
    }

    no_hp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Phone Number',
    }

    alamat = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Address',
    }

    domisili = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Domicile',
    }

    bank = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Bank Institute',
    }

    rekening = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Account Number',
    }

    pemilik = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Owner Name',
    }
    
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))
    npm = forms.CharField(label='NPM', required=True, max_length=50, widget=forms.TextInput(attrs=npm))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.TextInput(attrs=email))
    nama = forms.CharField(label='Nama Lengkap', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    no_telp = forms.CharField(label='Nomor Telepon', required=False,max_length=50, widget=forms.TextInput(attrs=no_hp))
    alamat_tinggal = forms.CharField(label='Alamat Tempat Tinggal', required=True,max_length=50, widget=forms.TextInput(attrs=alamat))
    alamat_domisili = forms.CharField(label='Alamat Domisili', required=True, max_length=50, widget=forms.TextInput(attrs=domisili))
    nama_bank = forms.CharField(label='Instansi Bank', required=True, max_length=50, widget=forms.TextInput(attrs=bank))
    no_rekening = forms.CharField(label='Nomor Rekening', required=True, max_length=50, widget=forms.TextInput(attrs=rekening))
    nama_pemilik = forms.CharField(label='Nama Pemilik Rekening', required=True, max_length=50, widget=forms.TextInput(attrs=pemilik))
from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages
# Create your views here.

def index(request):
    response = {}
    response['form_regmhs'] = RegisterFormMahasiswa
    return render(request, 'registermhs_landing.html', response)

@csrf_exempt
def register_mahasiswa(request): 

	form = RegisterFormMahasiswa(data=request.POST)

	if (request.method == 'POST' and form.is_valid()):
		cursor = connection.cursor()
	
		username = request.POST.get('username', False)
		password = request.POST.get('password', False)
		npm =request.POST.get('npm', False)
		email = request.POST.get('email', False)
		nama = request.POST.get('nama', False)
		no_telp = request.POST.get('no_telp', False)
		alamat_tinggal = request.POST.get('alamat_tinggal', False)
		alamat_domisili = request.POST.get('alamat_domisili', False)
		nama_bank = request.POST.get('nama_bank', False)
		no_rekening = request.POST.get('no_rekening', False)
		nama_pemilik = request.POST.get('nama_pemilik', False)

		query_pengguna = "insert into PENGGUNA (username, password, role) values ("+"'"+str(username)+"'"+", "+"'"+str(password)+"'"+", "+"'"+"mahasiswa"+"')"
		cursor.execute(query_pengguna)

		query_mhs = "insert into MAHASISWA (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) values ("+"'"+str(npm)+"'"+", "+"'"+str(email)+"'"+", "+"'"+str(nama)+"'"+", "+"'"+str(no_telp)+"'"+", "+"'"+str(alamat_tinggal)+"'"+", "+"'"+str(alamat_domisili)+"'"+", "+"'"+str(nama_bank)+"'"+", "+"'"+str(no_rekening)+"'"+", "+"'"+str(nama_pemilik)+"'"+", "+"'"+str(username)+"')"
		cursor.execute(query_mhs)

		return HttpResponseRedirect(reverse('login:index'))
	else:
		return HttpResponse("null")

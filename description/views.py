from django.shortcuts import render
from django.db import connection

# Create your views here.

response = {}
def index(request): 
    #kode = "3"
    query = request.GET.get('kode')
    kode = "{}".format(query)
    
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM skema_beasiswa WHERE kode ='+kode)
    sb = cursor.fetchall()
    response['Skema_Beasiswa'] = sb

    cursor.execute('SELECT * FROM syarat_beasiswa WHERE kode_beasiswa='+kode)
    syb = cursor.fetchall()
    response['Syarat_Beasiswa'] = syb

    #response['Skema_Beasiswa'] = SkemaBeasiswa.objects.raw("SELECT * FROM skema_beasiswa WHERE kode="+kode)
    #response['Syarat_Beasiswa'] = SyaratBeasiswa.objects.raw("SELECT * FROM syarat_beasiswa WHERE kode_beasiswa="+kode)

    return render(request, 'description.html', response)


from django.shortcuts import render
from django.db import connection
# Create your views here.

def index(request):
    response = {}
    return render(request, 'calon_penerima.html', response)  

def terima(request, no_urut, kode_beasiswa, npm):
	cursor = connection.cursor()
	cursor.execute("UPDATE pendaftaran SET status_terima='DITERIMA' WHERE no_urut='" +no_urut+ "' AND kode_skema_beasiswa='" +kode_beasiswa+ "' AND npm='" +npm+ "'")

	return detail(request, kode_beasiswa)


def tolak(request, no_urut, kode_beasiswa, npm):
	cursor = connection.cursor()
	cursor.execute("UPDATE pendaftaran SET status_terima='DITOLAK' WHERE no_urut='" +no_urut+ "' AND kode_skema_beasiswa='" +kode_beasiswa+ "' AND npm='" +npm+ "'")

	return detail(request, kode_beasiswa)

from django import forms
from .models import *

class RegisterFormYayasan(forms.Form):
    error_message = {
        'required': 'This field is required to fill',
    }

    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Username',
    }

    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter Password',
    }

    no_identitas = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter ID Number',
    }

    no_sk_yayasan = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Foundation Number',
    }
 
    email = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Email',
    }

    nama = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Foundation Name',
    }

    no_telp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Phone Number',
    }

    npwp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Account Number',
    }

    alamat = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Address',
    }

    
    
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))
    no_identitas = forms.CharField(label='Nomor Identitas', required=True, max_length=50, widget=forms.TextInput(attrs=no_identitas))
    no_sk_yayasan = forms.CharField(label='Nomor SK Yayasan', required=True, max_length=50, widget=forms.TextInput(attrs=no_sk_yayasan))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.TextInput(attrs=email))
    nama = forms.CharField(label='Nama Yayasan', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    no_telp = forms.CharField(label='Nomor Telepon CP', required=False,max_length=50, widget=forms.TextInput(attrs=no_telp))
    npwp = forms.CharField(label='NPWP', required=True, max_length=50, widget=forms.TextInput(attrs=npwp))
    alamat = forms.CharField(label='Alamat Yayasan', required=True,max_length=50, widget=forms.TextInput(attrs=alamat))
    
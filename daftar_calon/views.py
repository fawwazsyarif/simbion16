from django.db import connection
from django.http import HttpRequest
from datetime import datetime
from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.contrib import messages

# Create your views here.

response={}
def index(request):
	cursor = connection.cursor()
	username = request.session["username"]
	cursor.execute("SELECT role FROM pengguna WHERE username='" + username+"'")
	response['role'] = cursor.fetchone()[0]

	cursor.execute("SELECT npm, nama, email, no_telp FROM mahasiswa WHERE username='" + username+"'")
	response['npm'] = cursor.fetchone()[0]

	cursor.execute("SELECT npm, nama, email, no_telp FROM mahasiswa WHERE username='" + username+"'")
	response['email'] = cursor.fetchone()[2]

	cursor.execute("SELECT kode, nama, tgl_mulai_pendaftaran from skema_beasiswa_aktif JOIN skema_beasiswa ON kode_skema_beasiswa = kode WHERE status = 'TRUE' ORDER BY kode")
	response['arrBeasiswaAktif'] = cursor.fetchall()

	return render(request, 'calon.html', response)
from django import forms
from .models import *

class RegisterFormMahasiswa(forms.Form):
    error_message = {
        'required': 'This field is required to fill',
    }

    ips = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter IPS',
    }
    ips = forms.CharField(label='Indeks Prestasi Semester', required=True, max_length=50, widget=forms.TextInput(attrs=ips))
   
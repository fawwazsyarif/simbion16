from django.apps import AppConfig


class RegisterSkemaBeasiswaConfig(AppConfig):
    name = 'register_skema_beasiswa'

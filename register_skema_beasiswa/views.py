from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages

# Create your views here.

def baru(request):
    response = {}
    return render(request, 'buat_beasiswa_baru.html', response)

@csrf_exempt
def register_baru(request):
    form = RegisterFormBaru(data=request.POST)

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()

        kode = request.POST.get('kode', False)
        nama = request.POST.get('nama', False)
        jenis =request.POST.get('jenis', False)
        deskripsi = request.POST.get('deskripsi', False)
        syarat = request.POST.get('syarat', False)

        query_skema_beasiswa = "insert into skema_beasiswa (kode, nama, jenis, deskripsi) values ("+"'"+str(kode)+"'"+", "+"'"+str(nama)+"'"+", "+"'"+str(jenis)+"'"+", "+"'"+str(deskripsi)+"')"
        cursor.execute(query_skema_beasiswa)

        query_syarat_beasiswa = "insert into syarat_beasiswa (kode_beasiswa, syarat) values ("+"'"+str(kode)+"'"+", "+"'"+str(syarat)+"')"
        cursor.execute(query_syarat_beasiswa)

        return HttpResponseRedirect(reverse('home:index'))
    else:
        return HttpResponse("null")

def paket(request):
    response = {}
    return render(request, 'buat_beasiswa_dari_paket.html', response)
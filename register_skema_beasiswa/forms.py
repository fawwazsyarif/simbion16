from django import forms

class RegisterFormBaru(forms.Form):
    error_message = {
        'required': 'This field is required to fill',
    }

    kode = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Kode',
    }

    nama = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter Nama',
    }

    jenis = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Jenis',
    }
 
    deskripsi = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Deskripsi',
    }

    syarat = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Syarat',
    }
    
    kode = forms.CharField(label='Kode', required=True, max_length=50, widget=forms.TextInput(attrs=kode))
    nama = forms.CharField(label='Nama', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    jenis = forms.CharField(label='Jenis', required=True, max_length=50, widget=forms.TextInput(attrs=jenis))
    deskripsi = forms.CharField(label='Deskripsi', required=True, max_length=50, widget=forms.TextInput(attrs=deskripsi))
    syarat = forms.CharField(label='Syarat', required=True, max_length=50, widget=forms.TextInput(attrs=syarat))
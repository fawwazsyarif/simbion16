from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages

# Create your views here.

def index(request):
    response = {}
    response['form_login'] = FormLogin
    return render(request, 'login_landing.html', response)

@csrf_exempt
def login(request):
    form = FormLogin(data=request.POST)
    if (request.method == 'POST' and form.is_valid()):
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)

        query = "SELECT username FROM pengguna WHERE username = " + "'" + str(username) + "'" + " and password  = " + "'" + str(password) + "'"
        query_uname = "SELECT username FROM pengguna WHERE username = " + "'" + str(username) + "'"
        query_role = "SELECT role FROM pengguna WHERE username = " + "'" + str(username) + "'"

        cursor = connection.cursor()

        cursor.execute(query)
        get_user = cursor.fetchone()

        cursor.execute(query_uname)
        get_uname = cursor.fetchone()

        cursor.execute(query_role)
        get_role = cursor.fetchone()
        
        if (get_user is not None):
            if (get_role[0] == 'mahasiswa'):
                request.session['username'] = username
                request.session['ket_login'] = "login_mhs"
                return HttpResponseRedirect(reverse('home:index'))

            elif (get_role[0] == 'admin'):
                request.session['username'] = username
                request.session['ket_login'] = "login_adm"
                return HttpResponseRedirect(reverse('home:index'))

            elif (get_role[0] == 'donatur'):
                request.session['username'] = username
                request.session['ket_login'] = "login_dnt"
                return HttpResponseRedirect(reverse('home:index'))

        elif (get_uname is not None and get_user is None):
            messages.error(request, "Username atau password salah")
            return HttpResponseRedirect(reverse('login:index'))

        else:
            messages.error(request, "Anda belum terdaftar")
            return HttpResponseRedirect(reverse('login:index'))

def logout(request):
    try:
        request.session.flush()
    except KeyError:
        pass
        
    return HttpResponseRedirect(reverse('home:index'))


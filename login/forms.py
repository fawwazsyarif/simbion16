from django import forms
from .models import *

class FormLogin(forms.Form):
    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Username',
    }
    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter Password',
    }
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))

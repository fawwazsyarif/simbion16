from django import forms
from .models import *

class RegisterFormIndividu(forms.Form):
    error_message = {
        'required': 'This field is required to fill',
    }

    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Username',
    }

    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter Password',
    }

    no_identitas = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter ID Number',
    }

    nik = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter NIK',
    }
 
    email = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Email',
    }

    nama = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Full Name',
    }

    npwp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter NPWP',
    }

    no_telp = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Phone Number',
    }

    alamat = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter Address',
    }
    
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))
    no_identitas = forms.CharField(label='Nomor Identitas', required=True, max_length=50, widget=forms.TextInput(attrs=no_identitas))
    nik = forms.CharField(label='NIK', required=True, max_length=50, widget=forms.TextInput(attrs=nik))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.TextInput(attrs=email))
    nama = forms.CharField(label='Nama Lengkap', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    npwp = forms.CharField(label='NPWP', required=True, max_length=50, widget=forms.TextInput(attrs=npwp))
    no_telp = forms.CharField(label='Nomor Telepon', required=False,max_length=50, widget=forms.TextInput(attrs=no_telp))
    alamat = forms.CharField(label='Alamat Lengkap', required=True,max_length=50, widget=forms.TextInput(attrs=alamat))
    
from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages

# Create your views here.
def index(request):
    response = {}
    response['form_regind'] = RegisterFormIndividu
    return render(request, 'individu_landing.html', response)

@csrf_exempt
def register_individu(request):
	form = RegisterFormIndividu(data=request.POST)

	if (request.method == 'POST' and form.is_valid()):
		cursor = connection.cursor()

		username = request.POST.get('username', False)
		password = request.POST.get('password', False)
		no_identitas =request.POST.get('no_identitas', False)
		nik =request.POST.get('nik', False)
		email = request.POST.get('email', False)
		nama = request.POST.get('nama', False)
		npwp = request.POST.get('npwp', False)
		no_telp = request.POST.get('no_telp', False)
		alamat = request.POST.get('alamat', False)

		query_pengguna = "insert into PENGGUNA (username, password, role) values ("+"'"+str(username)+"'"+", "+"'"+str(password)+"'"+", "+"'"+"donatur"+"')"
		cursor.execute(query_pengguna)

		query_dnt = "insert into DONATUR (nomor_identitas, email, nama, npwp, no_telp, alamat, username) values ("+"'"+str(no_identitas)+"'"+", "+"'"+str(email)+"'"+", "+"'"+str(nama)+"'"+", "+"'"+str(npwp)+"'"+", "+"'"+str(no_telp)+"'"+", "+"'"+str(alamat)+"'"+", "+"'"+str(username)+"')"
		cursor.execute(query_dnt)

		query_ind = "insert into INDIVIDUAL_DONOR (nik, nomor_identitas_donatur) values ("+"'"+str(nik)+"'"+", "+"'"+str(no_identitas)+"')"
		cursor.execute(query_ind)

		return HttpResponseRedirect(reverse('login:index'))
	else:
		return HttpResponse("null")